var bodyY;
function scrollStop(){
	bodyY = $(window).scrollTop();
	$('html, body').addClass("no-scroll");
	$('.common').css("top",-bodyY);
}

function scrollStart(){
	$('html, body').removeClass("no-scroll");
	$('.common').css('top','auto');
	bodyY = $('html,body').scrollTop(bodyY);
}

function termsH(now){
	if ($('body').attr('data-mobile') == 'true') return false;
	var popupH = now.closest('.js-popup-h').outerHeight();
	var termsPadding = $('.terms-c').css('padding-top').replace(/[^0-9]/g, "");
	var termsTit = $('.terms-c__tit').outerHeight();
	var scrollH = popupH - (termsPadding*2) - termsTit;
	$('.terms-c__area').css('height',scrollH)
}
// $(window).on("load resize", function () {
// 	termsH();
// });
$('.js-popup-close').on('click',function(e){
	e.preventDefault();
	if ($('body').attr('data-mobile') == 'false' || $(this).attr('data-popup-type') == 'section' ){
		$('.detail__popup').fadeOut();
		$('.js-popup-open').removeClass('is-active');
		$(this).parents('.fs_section').find('.detail__dim').fadeToggle();
	}else{
		$('.detail__popup').fadeOut();
		scrollStart();
		$('.footer').removeClass('is-active');
		$(this).parents('.fs_section').find('.detail__dim').fadeToggle();
	}
});

$('.js-consultation').click(function(e) {
	e.preventDefault();
	$('.header__right').addClass('remove-phone');
	$('.popup-c.type-common').fadeIn(300);
	scrollStop();
});

$('.js-consultation-event').click(function(e) {
	e.preventDefault();
	$('.popup-c.type-event').fadeIn(300);
	$('.floating').removeClass('is-play');
	scrollStop();
});

$('.popup-c__close').on('click',function(e){
	e.preventDefault();
	$('.input1__clear').hide();
	$('.popup-c').fadeOut(300);
	$('.floating').addClass('is-play');
	scrollStart();
});

//sms
$('.js-sms-open').click(function(e) {
	e.preventDefault();
	$('.popup-sms').fadeIn(300);
	scrollStop();
});

$('.js-sms-close').on('click',function(e){
	e.preventDefault();
	$('.popup-sms').fadeOut(300);
	scrollStart();
});


//상담신청
$('.js-c-open').on('click',function(e){
	e.preventDefault();
	$('.popup-c').fadeIn(500);
});
$('.js-c-close').on('click',function(e){
	e.preventDefault();
	$('.popup-c').fadeOut(500);
});
$('.popup-tab__link').on('click',function(e){
	e.preventDefault();
	var tab_id = $(this).attr('data-tab');
	$('.popup-tab__item').removeClass('is-active');
	$(this).closest('.popup-c').find('.popup-content').removeClass('is-active');
	$(this).parent().addClass('is-active');
	$(".popup-content"+"."+tab_id).addClass('is-active');
});

$('.js-terms-open').on('click',function(e){
	e.preventDefault();
	var termsThis = $(this);
	$(this).closest('.popup-c').find('.popup-c__terms').addClass('is-active');
	termsH(termsThis);
});
$('.js-terms-close').on('click',function(e){
	e.preventDefault();
	$('.popup-c__terms').removeClass('is-active');
});


$('.popup-select__item').on('mouseenter',function(){
	$(this).addClass('is-hover');
}).on('mouseleave',function(){
	$(this).removeClass('is-hover');
});
function innerScrollStop(){
	$('.popup-c__box').addClass('no-scroll');
}
function innerScrollStart(){
	$('.popup-c__box').removeClass('no-scroll');
}
$(".popup-select__tit").click(function() {
	$('.popup-select, .popup-select2__subject, .popup-select__list').removeClass('is-active');
	$(this).parent().addClass('is-active');
	$(this).parent().find('.popup-select2__subject, .popup-select__list').addClass('is-active');
	innerScrollStop();
    return false;
});

$(".popup-select2__subject").click(function() {
	$(this).parent().removeClass('is-active');
	$(this).parent().find('.popup-select2__subject, .popup-select__list').removeClass('is-active');
innerScrollStart();    return false;
});



//온라인상담의 상담항목 선택
$(".popup-select__item").click(function() {
	$(this).parents('.popup-select__list').removeClass('is-active').parents(".popup-select").removeClass('is-active').find(".popup-select__tit").text($(this).text());
	$(this).parents('.popup-select').find('.popup-select2__subject').removeClass('is-active');
	$("#onlineForm .popup-select__item").removeClass('is-select');
	$(this).addClass('is-select');
});


//비용상담의 상담항목 선택
$(".popup-content.type-event .popup-select__item").on('click',function(){
    $(this).parents('.popup-select__list').removeClass('is-active').parents(".popup-select").removeClass('is-active').find(".popup-select__tit").text($(this).text());
    $(this).parents('.popup-select').find('.popup-select2__subject').removeClass('is-active');
    var selectType = $(this).parents('.popup-select').attr('data-style');
    if(selectType == 'multi'){
        var optCount = $('#eventForm .popup-check li').length;
        if(optCount > 2) {
            alert('최대 3개까지 선택 가능합니다.');
            return false;

        }else {
            if(!$(this).find('h4').hasClass('popup-select2__tit') && !$(this).hasClass('is-select')) {
                $(this).addClass('is-select');
                var idx = $(this).attr('id').split('_');
                var nowTxt = $(this).text();
                $('#part_item2').val(nowTxt).keyup();
                $('.popup-check').append('<li class="popup-check__item" id="cost_'+idx[1]+'"><span class="popup-check__wrap">'+nowTxt+'<a href="" class="popup-check__close">삭제</a></span></li>');
                return false;
            }
        }
    }
});

$(document).on('click','.popup-check__close',function(e){
	e.preventDefault();
	$(this).closest('.popup-check__item').remove();
})
