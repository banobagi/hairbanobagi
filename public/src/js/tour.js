

$('.js-tour-slide').slick({
    autoplay:false,
    autoplaySpeed:4000,
    slidesToShow:2,
    slidesToScroll: 2,
    dots:true,
    pauseOnHover:false,
    dotsClass:'tour__dot',
	prevArrow:'.js-tour-prev',
	nextArrow:'.js-tour-next',
	zIndex:10,
	responsive: [
        {
            breakpoint: 1366,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1
            }
        }
    ]
});
