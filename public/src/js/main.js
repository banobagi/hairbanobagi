






$('.js-banner-slide').slick({
    autoplay:false,
    autoplaySpeed:4000,
    slidesToShow:1,
    slidesToScroll: 1,
    arrows:false,
    dots:true,
    pauseOnHover:false,
    dotsClass:'main-banner__dot slide-dot',
    appendDots:$('.main-banner .slide-play__slide')
});

$('.js-content-slide').slick({
    autoplay:false,
    autoplaySpeed:4000,
    slidesToShow:1,
    slidesToScroll: 1,
    arrows:false,
    dots:true,
    pauseOnHover:false,
    dotsClass:'main-content__dot slide-dot',
    appendDots:$('.main-content .slide-play__slide')
});

$('.main-banner__btn').on('click',function(){
    if($(this).hasClass('is-pause')){
        $(this).removeClass('is-pause');
        $('.js-for-slide').slick('slickPause');
    }else{
        $(this).addClass('is-pause');
        $('.js-for-slide').slick('slickPlay');

    }
});

$('.main-content__btn').on('click',function(){
    if($(this).hasClass('is-pause')){
        $(this).removeClass('is-pause');
        $('.js-content-slide').slick('slickPause');
    }else{
        $(this).addClass('is-pause');
        $('.js-content-slide').slick('slickPlay');
    }
});



//디테일리스트 썸네일 오버효과
// $('.detail-thumbnail__link').on('mouseenter',function(){
// 	$(this).closest('.detail-thumbnail__item').addClass('is-hover');
// }).on('mouseleave',function(){
// 	$(this).closest('.detail-thumbnail__item').removeClass('is-hover');
// });



//디테일리스트
// $('.js-detail-slide').off('beforeChange').on('beforeChange', function(event, slick, currentSlide, nextSlide){
//     // var thumbnailNum = thumbnailItem.attr('data-page');
//     console.log(nextSlide)
//     $('.detail-thumbnail__item').eq(nextSlide).addClass('is-active').siblings().removeClass('is-active');
//     if(nextSlide == 0){
//         $('.detail-thumbnail__link.type-prev').trigger('click');
//     }else if(nextSlide == 3){
//         $('.detail-thumbnail__link.type-next').trigger('click');
//     }
// });
// $('.js-thumbnail-slide').off('afterChange').on('afterChange', function(event, slick, currentSlide, nextSlide){
//     if(currentSlide == 3){
//         $('.detail-thumbnail__link.type-prev').addClass('is-active');
//         $('.detail-thumbnail__link.type-next').removeClass('is-active');
//     }else{
//         $('.detail-thumbnail__link.type-next').addClass('is-active');
//         $('.detail-thumbnail__link.type-prev').removeClass('is-active');
//     }
// });
// $('.detail-thumbnail__link.type-next').addClass('is-active');
// $('.detail-thumbnail__btn').on('click', function(e){
// 	var thumbnailItem = $(this).closest('.detail-thumbnail__item');
// 	var thumbnailNum = thumbnailItem.attr('data-page')
//     thumbnailItem.addClass('is-active').siblings().removeClass('is-active');
// 	$('.js-detail-slide').slick('slickGoTo', thumbnailNum);
// });
// $('.detail-thumbnail__btn').on('mouseenter',function(){
//     $(this).closest('.detail-thumbnail__item').addClass('is-hover');
// }).on('mouseleave',function(){
//     $(this).closest('.detail-thumbnail__item').removeClass('is-hover');
// });

//카운트
var one = 'false';
function mainCounter(){
    if(one == 'false'){
        $('.js-count1').animateNumber({ number: 19 },1800);
        $('.js-count2').animateNumber({ number: 3000 },1800);
        $('.js-count3').animateNumber({ number: 365 },1800);
        one = 'true';
        console.log(one);
    }
}

const countElem = document.querySelector('.main-number');
function showValue(){
	var posX = countElem.getBoundingClientRect().left;
    var posY = countElem.getBoundingClientRect().top;
    var winW = $(window).width();
    var winH = $(window).height();
    var countH = $('.main-number').height();

    if ($('body').attr('data-mobile') == 'false'){
        if(posX < winW - 400){
    		mainCounter();
    	}
        $('.main_visual_bg_img').attr('data-swiper-parallax-y','500')
    }else{
        if(posY < winH - countH){
    		mainCounter();
    	}
        $('.main_visual_bg_img').attr('data-swiper-parallax-y','0')
    }

}
$(window).on('scroll resize load',function(){
	showValue();
});


function headerBg(){
	var winH = $(window).height();
	if ($(window).scrollTop()<winH) {
		$('.header').stop().removeClass('is-black');
	}
	else {
		$('.header').stop().addClass('is-black');
	}
}
$(window).scroll(function() {
	headerBg();
});

$('.main-for__body').on('mouseenter',function(){
    if ($('body').attr('data-mobile') == 'false'){
        var itemH = $(this).outerHeight();
        var innerBoxH = $(this).find('.main-for__scroll').outerHeight();
        var marginT = innerBoxH - itemH;
        $(this).find('.main-for__scroll').css('margin-top',-marginT);
    }

}).on('mouseleave',function(){
    if ($('body').attr('data-mobile') == 'false'){
        $(this).find('.main-for__scroll').css('margin-top',0);
    }
});


var detailTimer = setInterval(detailMotion, 5000);
$('.detail-thumbnail__item').on('click',function(){
    var pageNum = $(this).attr('data-page');
    $(this).addClass('is-active').siblings().removeClass('is-active');
    $('.detail-big__item').eq(pageNum-1).addClass('is-active').siblings().removeClass('is-active');
    clearInterval(detailTimer);
	detailTimer = setInterval(detailMotion, 5000);
})

function detailMotion() {
	var detailItem = $('.detail-thumbnail__item');
    var detailBig = $('.detail-big__item');
	var detailNum = $('.detail-thumbnail__item.is-active').index();
    console.log(detailNum)
	if(detailNum == 2){
		detailNum = -1;
	}
	detailItem.eq(detailNum+1).addClass('is-active').siblings().removeClass('is-active');
    detailBig.eq(detailNum+1).addClass('is-active').siblings().removeClass('is-active');
}



// Main visual
function main_visual(){

	// Timer
	var time = 4000;
	var transition_time = 1200;
	var time_with_trans = (time+transition_time)/1000;
	var timer = new TweenMax.to('.main-slide__timer-bar',time/1000,{scaleX:1,ease:Linear.easeNone,onComplete: function(){
		  main_bg_swiper.slideNext();
		}
	});

	timer.pause();

	var main_bg_swiper = new Swiper('.main-bg', {
		direction: 'horizontal',
		parallax: true,
		speed : transition_time,
        loop : true,
		pagination: {
		    el: '.main-slide__pagination',
			type: 'fraction',
		},
		navigation: {
			nextEl: '.slide-btn__btn.type-next',
			prevEl: '.slide-btn__btn.type-prev',
		},
		on : {
			init: function () {
				timer.restart();
			},
			transitionStart : function(){
				timer.duration(time_with_trans)
				timer.restart();
			}
		},
        breakpoints: {
            1366: {
                direction: 'vertical',
                updateOnWindowResize:true
            }
        }
	});

	var main_txt_swiper = new Swiper('.main-txt', {
		direction: 'horizontal',
		parallax: true,
		loop : true,
        speed : transition_time,
        // breakpointsInverse:true,
        breakpoints: {
            1366: {
                direction: 'vertical'
            }
        }

	});

	main_bg_swiper.controller.control = main_txt_swiper;
	main_txt_swiper.controller.control = main_bg_swiper;
}


main_visual();


var titMotionNumPc = 1;
var titMotionNumMo = 1;
$(window).on("load resize", function () {
    //ie 영상
    var videoW = $('.main-video').outerWidth()/2;
    var videoH = $('.main-video__video').outerHeight()/2;
    //ie 영상 끝
    if ($('body').attr('data-mobile') == 'false'){
        if ( $('.js-for-slide').hasClass('slick-initialized')){
             $('.js-for-slide').slick('unslick');
        }
        //ie 영상
        if ($('body').attr('data-wide') == 'true'){
            $('.main-video__video').addClass('is-wide').css({'margin-top':-videoH,"margin-left":"0"});
        }else{
            $('.main-video__video').removeClass('is-wide').css({'margin-left':-videoW,"margin-top":"0"});
        }
        //ie 영상 끝
    }else{
        if ( !$('.js-for-slide').hasClass('slick-initialized')){
             $('.js-for-slide').slick({
                arrows:false,
                zIndex:50,
                dots:true,
                centerMode: true,
                centerPadding: '0',
                dotsClass: 'custom_paging',
                customPaging: function (slider, i) {
                    var totalSlides = slider.slideCount;
                    $('.main-for__total').text(totalSlides)
                }
            });
        }

        $('.js-for-slide').on('afterChange', function (event, slick, currentSlide, nextSlide) {
            var i = (currentSlide ? currentSlide : 0) + 1;
            $('.main-for__current').text(i);
            $('.main-for__total').text(slick.slideCount);
        });
        //ie 영상
        $('.main-video__video').removeClass('is-wide').css({'margin-left':-videoW,"margin-top":"0"});
        //ie 영상 끝
    }


    if ($('body').attr('only-mobile') == 'false'){
        titMotionPc();
    }else{
        titMotionMo();
    }

    //main 영상 텍스트
    function titMotionPc(){
        if(titMotionNumPc == 1){
            var $txt = $('.main-video__spelling.hidden-m');
            var $tit = $('.main-video__subject');
            TweenMax.set( $tit, { opacity: 0 } );
            TweenLite.to( $tit, 5, { opacity: 1, ease: Power2.easeOut } );
            TweenMax.set( $txt, { opacity: 0 } );
            TweenMax.staggerTo( $txt, 1.5, { delay: 1, opacity: 1, ease: Power1.easeOut }, 0.1 );
            titMotionNumPc++;
        }
    }
    function titMotionMo(){
        if(titMotionNumMo == 1){
            var $txt = $('.main-video__spelling.hidden-pt');
            var $tit = $('.main-video__subject');
            TweenMax.set( $tit, { opacity: 0 } );
            TweenLite.to( $tit, 2, { opacity: 1, ease: Power2.easeOut } );
            TweenMax.set( $txt, { opacity: 0 } );
            TweenMax.staggerTo( $txt, 1.5, { delay: 1, opacity: 1, ease: Power1.easeOut }, 0.1 );
            titMotionNumMo++;
        }
    }


});

$(window).on("load", function () {
    var bannerNum = $('.main-banner__item').length;
    if(bannerNum == 1){
        $('.slide-play__bottom').hide();
    }
});
