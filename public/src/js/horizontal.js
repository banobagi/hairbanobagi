$('.header').stop().addClass('is-black');
$(window).on("load resize", function () {
    //횡스크롤 모션
	var elem = $.jInvertScroll(['.horizontal-scroll'],        // an array containing the selector(s) for the elements you want to animate
		{
		//height: 12000,                   // optional: define the height the user can scroll, otherwise the overall length will be taken as scrollable height
		onScroll: function(percent) {   //optional: callback function that will be called when the user scrolls down, useful for animating other things on the page
		}
	});

	if ($('body').attr('data-mobile') == 'false'){
		elem.reinitialize();
	}else{
		elem.destroy();
	}
    try {
        var footerLeft = leftValue('.horizontal-footer');
        $(window).scroll(function () {
            var winWidth = $(window).outerWidth();
            var total = $('.horizontal-scroll').width() + winWidth; // 푸터없을시  + winWidth 제거
            var leftX = Math.abs($('.horizontal-scroll').attr('data-left'));
            var perc = leftX / (total - $(window).width()) * 100;
            var height = $('html').height();
            var final = Math.ceil(((height - $(window).height())/ 100 * perc) + 20); //_20은 클릭시 해당세션에 네비 active 되게 좀더 넓게 영역잡을려고 추가함
            var lnbLeft = footerLeft - 200;
            if(final > footerLeft){
                $('.breadcrumb, .horizontal-nav').addClass('is-hide');
                try {$('.floating').addClass('is-hide')}catch(e){}
            }else{
                $('.breadcrumb, .horizontal-nav').removeClass('is-hide');
                try {$('.floating').removeClass('is-hide')}catch(e){}
            }
            if(final > lnbLeft){
                $('.lnb').addClass('is-hide');
            }else{
                $('.lnb').removeClass('is-hide');
            }
        });
    } catch(error) {}

});
function leftValue(target){
    var winWidth = $(window).outerWidth();
    var total = $('.horizontal-scroll').width() + winWidth; // 푸터없을시  + winWidth 제거
    var left = $(target).position().left;
    var headerW = $('.header').outerWidth() + 2;
    var perc = (left - headerW) / (total - $(window).width()) * 100;
    var height = $('html').height();
    var final = Math.ceil(((height - $(window).height())/ 100 * perc));
    return final;
}


enquire.register("screen and (max-width:1366px)", {
    match : function() {

    },unmatch : function() {
        window.location.reload(true);
    },setup  :  function () {
    },
    deferSetup :  true
});
