
$('.doctor-nav__btn').on('click',function(){
	var pageNum = $(this).attr('data-page');
	$(this).parents('.doctor-nav__item').addClass('is-active').siblings().removeClass('is-active');
	$('.content-doctor__item').eq(pageNum-1).addClass('is-active').siblings().removeClass('is-active');
	$('.doctor-photo__item:nth-child('+pageNum+')').addClass('is-active').siblings().removeClass('is-active')
})


$(window).on("load",function(){
	$('.js-fade-in').addClass('is-active');
})


$(window).on("load resize", function () {
	var winW = $(window).width();
    var coverLeft1 = leftValue('.cover-section1');
	var coverLeft2 = leftValue('.cover-section2');
	var coverLeft3 = leftValue('.cover-section3');
	var contentSection2 = leftValue('.content-section2');
	var coverEnd = leftValue('.cover-end');
    $(window).scroll(function () {
        var winWidth = $(window).outerWidth();
        var total = $('.horizontal-scroll').width() + winWidth; // 푸터없을시  + winWidth 제거
        var leftX = Math.abs($('.horizontal-scroll').attr('data-left'));
        var perc = leftX / (total - $(window).width()) * 100;
        var height = $('html').height();
        var final = Math.ceil(((height - $(window).height())/ 100 * perc) - 100); //_20은 클릭시 해당세션에 네비 active 되게 좀더 넓게 영역잡을려고 추가함
		if(final > coverEnd + 400){
			$('.horizontal-fix').addClass('is-hide');
			$('.horizontal-fix__item').removeClass('is-active');
		}else if(final > coverLeft3){
			$('.horizontal-fix').removeClass('is-hide');
            $('.horizontal-fix__item').eq(2).addClass('is-active').siblings().removeClass('is-active');
        }else if(final > coverLeft2 + 600){
			$('.horizontal-fix').removeClass('is-hide');
			$('.horizontal-fix__item').eq(1).addClass('is-active').siblings().removeClass('is-active');
		}else if(final > coverLeft1){
			$('.horizontal-fix').removeClass('is-hide');
			$('.horizontal-fix__item').eq(0).addClass('is-active').siblings().removeClass('is-active');
		}
		if(final > (contentSection2)){
			$('.breadcrumb').removeClass('is-video');
		}else{
			$('.breadcrumb').addClass('is-video');
		}
    });




	function showScroll(){
	    var scrollMotion = new Array();
	    var scrollMotionlNum = $('.js-scroll-motion').length;
		var winW = $(window).outerWidth();
		var winH = $(window).outerWidth();
	    for(var i=0;i<scrollMotionlNum;i++){
			if ($('body').attr('data-mobile') == 'false'){
				scrollMotion[i] = document.querySelectorAll('.js-scroll-motion')[i].getBoundingClientRect().left;
				if(scrollMotion[i] < (winW*0.8)){
					document.querySelectorAll('.js-scroll-motion')[i].classList.add('is-active')
				}
			}else{
				scrollMotion[i] = document.querySelectorAll('.js-scroll-motion')[i].getBoundingClientRect().top;
				if(scrollMotion[i] < (winH*1.35)){
					document.querySelectorAll('.js-scroll-motion')[i].classList.add('is-active')
				}
			}
	    }
	};
	$(window).on('scroll resize load',function(){
		showScroll();
	});

	try {
		if ($('body').attr('data-mobile') == 'false'){
	        if ( $('.js-table-slide').hasClass('slick-initialized')){
	             $('.js-table-slide').slick('unslick');
	         }
	    }else{
			if ( !$('.js-table-slide').hasClass('slick-initialized')){
	             $('.js-table-slide').slick({
	                autoplay:false,
	                autoplaySpeed:3000,
	                slidesToShow:1,
	                slidesToScroll: 1,
	                arrows:false,
	                dots:false,
	                pauseOnHover:false,
	                pauseOnFocus:false,
	                zIndex:10
	            });
	            $('.js-table-prev').on('click',function(e){
	                e.preventDefault();
	                $('.js-table-slide').slick("slickPrev");
	            });
	            $('.js-table-next').on('click',function(e){
	                e.preventDefault();
	                $('.js-table-slide').slick("slickNext");
	            });
	        }

	    }
	}catch(e){}

});



function leftValue(target){
    var winWidth = $(window).outerWidth();
    var total = $('.horizontal-scroll').width() + winWidth; // 푸터없을시  + winWidth 제거
    var left = $(target).position().left;
    var headerW = $('.header').outerWidth() + 2;
    var perc = (left - headerW) / (total - $(window).width()) * 100;
    var height = $('html').height();
    var final = Math.ceil(((height - $(window).height())/ 100 * perc));
    return final;
}




//백그라운드 fixed 고정
// if(navigator.userAgent.match(/Trident\/7\./)) {
// 	if ($('body').attr('data-mobile') == 'false') return false;
//     $('body').on("mousewheel", function (event,delta) {
//         event.preventDefault();
//
//         var wheelDelta = delta * 100;
//         var currentScrollPosition = window.pageYOffset;
//         window.scrollTo(0, currentScrollPosition - wheelDelta);
//     });
//
//     $('body').keydown(function (e) {
//         e.preventDefault();
//         var currentScrollPosition = window.pageYOffset;
//
//         switch (e.which) {
//
//             case 38: // up
//                 window.scrollTo(0, currentScrollPosition - 120);
//                 break;
//
//             case 40: // down
//                 window.scrollTo(0, currentScrollPosition + 120);
//                 break;
//
//             default: return;
//         }
//     });
// }
