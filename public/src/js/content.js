$(window).on("load resize", function () {
    if ($('body').attr('data-mobile') == 'false'){
        try {numberWidth();}catch(e){}
        if ( !$('.js-pick-slide').hasClass('slick-initialized')){
             $('.js-pick-slide').slick({
                autoplay:false,
                autoplaySpeed:3000,
                slidesToShow:4,
                slidesToScroll: 1,
                arrows:false,
                dots:false,
                pauseOnHover:false,
                pauseOnFocus:false,
                zIndex:10
            });
            $('.js-pick-prev').on('click',function(e){
                e.preventDefault();
                $('.js-pick-slide').slick("slickPrev");
            });
            $('.js-pick-next').on('click',function(e){
                e.preventDefault();
                $('.js-pick-slide').slick("slickNext");
            });
        }


    }else{
        if ( $('.js-pick-slide').hasClass('slick-initialized')){
             $('.js-pick-slide').slick('unslick');
         }
        try {
            if ( $('.js-how1-slide').hasClass('slick-initialized')){
              $('.js-how1-slide').slick('unslick');
            }
            if ( $('.js-how2-slide').hasClass('slick-initialized')){
               $('.js-how2-slide').slick('unslick');
            }
        }catch(e){}
    }

    //처음 로딩시 모션나오게하기
    setTimeout(function(){
        var nowY = window.pageYOffset;
        if(nowY == 0) return false;
        $('html, body').stop().animate({scrollTop: nowY+1},100);
    },200);


    var section = new Array();
    var horizontalNum = $('.horizontal-section').length;
    for(var i=0;i<horizontalNum;i++){
        section[i] = leftValue('.content-section'+(i+1));
    }
    var footerLeft = leftValue('.horizontal-footer');
    $(window).on('scroll',function(){
        var winWidth = $(window).outerWidth();
        var total = $('.horizontal-scroll').width() + winWidth; // 푸터없을시  + winWidth 제거
        var leftX = Math.abs($('.horizontal-scroll').attr('data-left'));
        var perc = leftX / (total - $(window).width()) * 100;
        var height = $('html').height();
        var final = Math.ceil(((height - $(window).height())/ 100 * perc) + 5); //_5은 클릭시 해당세션에 네비 active 되게 좀더 넓게 영역잡을려고 추가함
        // var final2 = final - 10;
        for(var i=0;i<horizontalNum;i++){
            if(final > section[i]){
                $('.horizontal-nav__item').eq(i).addClass('is-active').siblings().removeClass('is-active');
            }
        }
    });

});




(function(){
    var horizontalNum =  $('.horizontal-section').length;
    var sectionName = new Array();
    for(var i=1; i<(horizontalNum+1); i++) {
        sectionName[i] = $('.content-section'+(i)).attr('data-page');
        if(i == 1){
            $('.horizontal-nav').append('<li class="horizontal-nav__item is-active" data-target="'+i+'"><span class="horizontal-nav__txt">'+sectionName[i]+'</span></li>');
        }else{
            $('.horizontal-nav').append('<li class="horizontal-nav__item" data-target="'+i+'"><span class="horizontal-nav__txt">'+sectionName[i]+'</span></li>');
        }
    }
    $('.horizontal-nav__item').on('click',function(){
        var winWidth = $(window).outerWidth();
		var target = '.content-section'+$(this).data('target');
		var left = $(target).position().left;
        var headerW = $('.header').outerWidth() + 2;
		var total = $('.horizontal-scroll').width() + winWidth; // 푸터없을시  + winWidth 제거
		var perc = (left - headerW) / (total - $(window).width()) * 100;
		var height = $('html').height();
		var final = Math.ceil(((height - $(window).height())/ 100 * perc) + 5) ;
		$('html, body').stop().animate({scrollTop: final}, 1500,function(){
            $('html, body').stop().animate({scrollTop: final+5}, 100);
        });
    });


})();



function leftValue(target){
    var winWidth = $(window).outerWidth();
    var total = $('.horizontal-scroll').width() + winWidth; // 푸터없을시  + winWidth 제거
    var left = $(target).position().left;
    var headerW = $('.header').outerWidth() + 2;
    var perc = (left - headerW) / (total - $(window).width()) * 100;
    var height = $('html').height();
    var final = Math.ceil(((height - $(window).height())/ 100 * perc));
    return final;
}



const contentProcess = document.querySelector('.content-process');
const contentCare = document.querySelector('.content-section-care');
function showValue(){
	var processX = contentProcess.getBoundingClientRect().left;
    var processY = contentProcess.getBoundingClientRect().top;
    try {
        var careX = contentCare.getBoundingClientRect().left;
        var careY = contentCare.getBoundingClientRect().top;
    }catch(e){}
    // var posY = countElem.getBoundingClientRect().top;
    var winW = $(window).width();
    var winH = $(window).height();
    var headerW = $('.header').width();
    var countH = $('.main-info').height();

    if ($('body').attr('data-mobile') == 'false'){
        if(processX < winW/2){
    		processMotion();
    	}
        try {
            if(careX < winW/2){
                careMotion();
            }
        }catch(e){}

    }else{
        if(processY < winH/2){
    		processMotion();
    	}
        try {
            if(careY < 0){
                careMotion();
            }
        }catch(e){}
    }

}

$(window).on('scroll resize load',function(){
    var loadingNum =1;
    showScroll(1);
    showValue();
});

//디테일 너비잡아주기
function numberWidth(){
    var detailElem = $('.js-number-width .content-number__item:nth-child(2)');
    var detailMargin = detailElem.css('margin-left').replace(/[^0-9]/g, "");
    var detailAreaElem = $('.js-number-width .content-number__area');
    detailAreaElem.css('width','auto');
    var detailW = detailAreaElem.outerWidth()
    var detailAreaW = Number(detailW) + Number(detailMargin);
    detailAreaElem.css('width',detailAreaW);
}



$('.js-process').slick({
    fade:true,
    autoplay:false,
    autoplaySpeed:3000,
    slidesToShow:1,
    slidesToScroll: 1,
    arrows:false,
    dots:true,
    speed:0,
    pauseOnHover:false,
    pauseOnFocus:false,
    dotsClass:'content-process__dot',
    zIndex:10
    // appendDots:$('.main-banner .slide-play__slide')
});


var processOne = 'false';
function processMotion(){
    if(processOne == 'false'){
        $('.js-process').slick('slickPlay');
        $('.js-process .slick-active').next().addClass('is-bar');
        $(".js-process").on('afterChange',function(){
            $(this).slick('slickPlay'); //슬라이더 자동실행
            $('.content-process__dot li').removeClass('is-bar is-before')
            $(this).find('.slick-active').next().addClass('is-bar');
            $(this).find('.slick-active').addClass('is-before').prevAll('li').addClass('is-before')
        });
        processOne = 'true';
    }
}
var careOne = 'false';
function careMotion(){
    if(careOne == 'false'){
        var i = 0;
        careActive();
        function careActive(){
            var itemList = $('.content-year__item').length;
            if( i == itemList - 1){
                $('.content-year__noti').addClass('is-active');
            }
            $('.content-year__item, .content-bullet__item').eq(i).addClass('is-active');
            $('.content-bullet__item').eq(i).addClass('is-active');
            $('.hair-one__item').eq(i).addClass('is-active');
            i++;
        };
        var time = 2000;
        var itemList = $('.content-year__item').length;
        var interval = setInterval(careActive,time);
        var timeOut = (time * (itemList-1))+1000;
        setTimeout(function(){
            clearInterval(interval);
        },timeOut);
        careOne = 'true';
    }
}















$(window).on("load resize", function () {

});

$('.js-content-open').on('click',function(e){
    e.preventDefault();
    $(this).parents('.horizontal-section').find('.content-popup').fadeIn(300);
});

$('.js-content-close').on('click',function(e){
    e.preventDefault();
    $(this).parents('.horizontal-section').find('.content-popup').fadeOut(300);
})



function showScroll(target){
    var scrollMotion = new Array();
    var scrollMotionlNum = $('.js-scroll-motion').length;
    var winW = $(window).outerWidth();
    var winH = $(window).outerWidth();
    for(var i=0;i<scrollMotionlNum;i++){
        if ($('body').attr('data-mobile') == 'false'){
            scrollMotion[i] = document.querySelectorAll('.js-scroll-motion')[i].getBoundingClientRect().left;
            if(scrollMotion[i] < (winW*target)){
                document.querySelectorAll('.js-scroll-motion')[i].classList.add('is-active')
            }
        }else{
            scrollMotion[i] = document.querySelectorAll('.js-scroll-motion')[i].getBoundingClientRect().top;
            if(scrollMotion[i] < (winH*1.35)){
                document.querySelectorAll('.js-scroll-motion')[i].classList.add('is-active')
            }
        }
    }
};


function howSlide(){
    $(window).on("load resize", function () {

        if ($('body').attr('data-mobile') == 'false'){

            try {
                if ( !$('.js-how1-slide').hasClass('slick-initialized')){
                     $('.js-how1-slide').slick({
                        autoplay:false,
                        autoplaySpeed:3000,
                        slidesToShow:4,
                        slidesToScroll: 1,
                        arrows:false,
                        dots:false,
                        pauseOnHover:false,
                        pauseOnFocus:false,
                        zIndex:10
                    });
                    $('.js-how1-prev').on('click',function(e){
                        e.preventDefault();
                        $('.js-how1-slide').slick("slickPrev");
                    });
                    $('.js-how1-next').on('click',function(e){
                        e.preventDefault();
                        $('.js-how1-slide').slick("slickNext");
                    });
                }

                if ( !$('.js-how2-slide').hasClass('slick-initialized')){
                     $('.js-how2-slide').slick({
                        autoplay:false,
                        autoplaySpeed:3000,
                        slidesToShow:4,
                        slidesToScroll: 1,
                        arrows:false,
                        dots:false,
                        pauseOnHover:false,
                        pauseOnFocus:false,
                        zIndex:10
                    });
                    $('.js-how2-prev').on('click',function(e){
                        e.preventDefault();
                        $('.js-how2-slide').slick("slickPrev");
                    });
                    $('.js-how2-next').on('click',function(e){
                        e.preventDefault();
                        $('.js-how2-slide').slick("slickNext");
                    });
                }
            }catch(e){}

        }else{
            try {
                if ( $('.js-how1-slide').hasClass('slick-initialized')){
                  $('.js-how1-slide').slick('unslick');
                }
                if ( $('.js-how2-slide').hasClass('slick-initialized')){
                   $('.js-how2-slide').slick('unslick');
                }
            }catch(e){}
        }
    });
}

howSlide();
//tab-v1
$('.tab-v1__link').on('click',function(e){
	e.preventDefault();
	var tabClass = $(this).attr('href');
	$('.tab-v1__item, .tab-v1__content').removeClass('is-active');
	$(this).parents('.tab-v1__item').addClass('is-active');
	$("."+tabClass).addClass('is-active');

	// tab ���� �� matchHeight update
	$.fn.matchHeight._update();
    // howSlide();
});
