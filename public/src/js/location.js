var map1;
var map2;
function initMapKako(){
	var mapContainer = document.getElementById('map'), // 지도를 표시할 div
    mapOption = {
        center: new daum.maps.LatLng(37.502638, 127.035733), // 지도의 중심좌표
		draggable:false,
        level: 3// 지도의 확대 레벨
    };

	map1 = new daum.maps.Map(mapContainer, mapOption); // 지도를 생성합니다

	var imageSrc = 'http://banobagihair.cafe24.com/Pub/images/common/banobagi_marker.png', // 마커이미지의 주소입니다
		imageSize = new daum.maps.Size(170, 96), // 마커이미지의 크기입니다
		imageOption = {offset: new daum.maps.Point(100, 120)}; // 마커이미지의 옵션입니다. 마커의 좌표와 일치시킬 이미지 안에서의 좌표를 설정합니다.

	// 마커의 이미지정보를 가지고 있는 마커이미지를 생성합니다
	var markerImage = new daum.maps.MarkerImage(imageSrc, imageSize, imageOption),
		markerPosition = new daum.maps.LatLng(37.502638, 127.035733); // 마커가 표시될 위치입니다

	// 마커를 생성합니다
	var marker = new daum.maps.Marker({
		position: markerPosition,
		image: markerImage // 마커이미지 설정
	});

	// 마커가 지도 위에 표시되도록 설정합니다
	marker.setMap(map1);
}
function initMapKakoM(){
	var mapContainer = document.getElementById('map'), // 지도를 표시할 div
    mapOption = {
        center: new daum.maps.LatLng(37.502638, 127.035733), // 지도의 중심좌표
		draggable:false,
        level: 3// 지도의 확대 레벨
    };

	map2 = new daum.maps.Map(mapContainer, mapOption); // 지도를 생성합니다

	var imageSrc = 'http://banobagihair.cafe24.com/Pub/images/common/banobagi_marker_m.png', // 마커이미지의 주소입니다
		imageSize = new daum.maps.Size(124, 70), // 마커이미지의 크기입니다
		imageOption = {offset: new daum.maps.Point(100, 120)}; // 마커이미지의 옵션입니다. 마커의 좌표와 일치시킬 이미지 안에서의 좌표를 설정합니다.

	// 마커의 이미지정보를 가지고 있는 마커이미지를 생성합니다
	var markerImage = new daum.maps.MarkerImage(imageSrc, imageSize, imageOption),
		markerPosition = new daum.maps.LatLng(37.502638, 127.035733); // 마커가 표시될 위치입니다

	// 마커를 생성합니다
	var marker = new daum.maps.Marker({
		position: markerPosition,
		image: markerImage // 마커이미지 설정
	});

	// 마커가 지도 위에 표시되도록 설정합니다
	marker.setMap(map2);
}

function setZoomable(zoomable) {
	map2.setZoomable(zoomable);
}
$(window).ready(function() {
	if ($('body').attr('data-mobile') == 'false'){
		initMapKako();
	}else{
		initMapKakoM();
	}
	var mapTop = $('#map').offset().top;
	$(window).scroll(function() {
		if ($(window).scrollTop() < mapTop) {
			setZoomable(false);
		}
	});
	$('.map-motion__fake').click(function(){
		$(this).addClass('is-hide');
		$('.map-motion__bg').addClass('is-important');
		setZoomable(true);
	});
	$('.map-motion__bg').on('touchstart',function(){
		$('.map-motion__bg').addClass('is-active');
	});
	$('.map-motion__bg').on('touchend',function(){
		$('.map-motion__bg').removeClass('is-active');
	});
});
