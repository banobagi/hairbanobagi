$(window).on("load resize", function () {
	$('body').attr('data-mobile',
		(function(){
			var r = ($(window).width() <= 1366) ? true : false;
			return r;
		}())
	);
	$('body').attr('only-mobile',
		(function(){
			var r = ($(window).width() <= 680) ? true : false;
			return r;
		}())
	);

	$('body').attr('data-wide',
		(function(){
			var r = ($(window).height() <= 680) ? true : false;
			return r;
		}())
	);

	var bodyY;
	function scrollStop(){
		bodyY = $(window).scrollTop();
		$('html, body').addClass("no-scroll");
		$('.common').css("top",-bodyY);
	}

	function scrollStart(){
		$('html, body').removeClass("no-scroll");
		$('.common').css('top','auto');
		bodyY = $('html,body').scrollTop(bodyY);
	}


    function gnbPcOpen(){
        var gnbElem = $('.gnb');
        var htmlElem = $('html, body');
        var headerBtnElem = $(this);
        if(gnbElem.hasClass('is-active')){
            gnbElem.removeClass('is-active');
            headerBtnElem.removeClass('is-active');
            htmlElem.removeClass('no-scroll');
        }else{
            gnbElem.addClass('is-active');
            headerBtnElem.addClass('is-active');
            htmlElem.addClass('no-scroll');
        }
    }

    function gnbMoOpen(){
        var gnbElem = $('.m-gnb');
        var htmlElem = $('html, body');
        var headerBtnElem = $(this);
        if(gnbElem.hasClass('is-active')){
            gnbElem.removeClass('is-active');
            headerBtnElem.removeClass('is-active');
			$('.header').removeClass('is-active')
            scrollStart()
        }else{
            gnbElem.addClass('is-active');
            headerBtnElem.addClass('is-active');
			$('.header').addClass('is-active')
            scrollStop();
        }
    }
	//리사이즈시 스크롤 멈춘거 해제
	if ($('body').attr('data-mobile') == 'false'){
		if($('.js-m-btn').hasClass('is-active')){
			$('.m-gnb').removeClass('is-active');
            $('.js-m-btn').removeClass('is-active');
            scrollStart()
		}
	}else{
		if($('.js-gnb-btn').hasClass('is-active')){
			$('.gnb').removeClass('is-active');
            $('.js-gnb-btn').removeClass('is-active');
            $('html, body').removeClass('no-scroll');
		}
	}
	$('.js-gnb-btn').off('click').on('click',gnbPcOpen).on('mouseenter',function(){
		$(this).addClass('is-hover');
	}).on('mouseleave',function(){
		$(this).removeClass('is-hover');
	})
	$('.js-m-btn').off('click').on('click',gnbMoOpen);


	//박스 높이값 맞추기
	$('.js-height').matchHeight({
		byRow: true,
		target: null
	});

	gnbScroll();

});



$('.gnb__depth1-link').on('click',function(e){
    var gnbPage = $(this).attr('gnb-page');
    var gnbDepth = $(this).closest('.gnb__depth1-item').attr('data-depth');
    if(gnbDepth != "1"){
        e.preventDefault();
		$('.gnb__depth1-list').addClass('is-active')
        $(this).closest('.gnb__depth1-item').addClass('is-active').siblings().removeClass('is-active');
    	$('.gnb__container').removeClass('is-active');
    	$("."+gnbPage).addClass('is-active');
    }
});

$('.gnb__depth1-link').on('click',function(e){
    var gnbPage = $(this).attr('gnb-page');
    var gnbDepth = $(this).closest('.gnb__depth1-item').attr('data-depth');
    if(gnbDepth != "1"){
        e.preventDefault();
		$('.gnb__depth1-list').addClass('is-active')
        $(this).closest('.gnb__depth1-item').addClass('is-active').siblings().removeClass('is-active');
    	$('.gnb__container').removeClass('is-active');
    	$("."+gnbPage).addClass('is-active');
    }
});

// $('.lnb__link').on('click',function(e){
//     var lnbPage = $(this).attr('lnb-page');
//     e.preventDefault();
//     $(this).closest('.lnb__item').addClass('is-active').siblings().removeClass('is-active');
// 	$('.event').removeClass('is-active');
// 	$("."+lnbPage).addClass('is-active');
// });


$('.m-gnb__depth1-link').on('click',function(e){
    var gnbDepth = $(this).closest('.m-gnb__depth1-item').attr('data-depth');
	if(gnbDepth != "1"){
	    e.preventDefault();
	}
});
//progressBar
$.progressIndicator({
   direction : 'top',
   barColor: 'rgb(255, 242, 34)',
   percentageEnabled : false,
   percentageColor: '#222',
   easingSpeed : 0.5,
   height: 5,
   target : 'html', // selector
   onStart : function(){
   },
   onEnd : function(){
   },
   onProgress : function(perecent){
   }
});







$('.js-select-v1').on('click',function(){
	var selectH = $(this).parent().find('.select-v1__list').outerHeight();
	$(this).parent().find('.select-v1__list').css('top',-selectH)
	$(this).parent().toggleClass('is-active');
});


//갤러리아 버튼모션
$('.cok__item').on('mouseenter',function(){
	if ($('body').attr('data-mobile') == 'true') return false;
    $(this).removeClass('is-hover-end is-hover-start');
    $(this).addClass('is-hover-start');
}).on('mouseleave',function(){
	if ($('body').attr('data-mobile') == 'true') return false;
    $(this).addClass('is-hover-end');
});

$('.js-btn-v1').on('mouseenter',function(){
	if ($('body').attr('data-mobile') == 'true') return false;
	$(this).removeClass('is-hover-end is-hover-start');
    $(this).addClass('is-hover-start');
}).on('mouseleave',function(){
	if ($('body').attr('data-mobile') == 'true') return false;
    $(this).addClass('is-hover-end');
});

$('.more-v1').on('mouseenter',function(){
	if ($('body').attr('data-mobile') == 'true') return false;
    $(this).addClass('is-hover');
}).on('mouseleave',function(){
	if ($('body').attr('data-mobile') == 'true') return false;
    $(this).removeClass('is-hover');
});


$(".img-hover").on('mouseenter',function(){
     $(this).attr("src", $(this).attr("src").replace("_before","_after"));
}).on('mouseleave',function(){
     $(this).attr("src", $(this).attr("src").replace("_after", "_before"));
});

$(".footer-visit__link").on('mouseenter',function(){
     $(this).parents('.footer-visit__item').addClass('is-hover');
}).on('mouseleave',function(){
	$(this).parents('.footer-visit__item').removeClass('is-hover');
});





$('.js-top').on('click', function(e) {
   e.preventDefault();
   $('html, body').stop().animate({
	   scrollTop: 0
   }, 500);
});

$('.js-hover').on('mouseenter',function(){
	if ($('body').attr('data-mobile') == 'true') return false;
	$(this).addClass('is-hover');
}).on('mouseleave',function(){
	if ($('body').attr('data-mobile') == 'true') return false;
	$(this).removeClass('is-hover');
})



$('.js-tab-btn').on('click', function(){
	//var p = $(this).parents('[data-product]');
	var state = $('.select-v2__list');
	if(state.hasClass('is-active')){
		$(this).removeClass('is-active')
		state.removeClass('is-active')
	} else {
		$(this).addClass('is-active')
		state.addClass('is-active')
	}
});
//이벤트 탭
$('.select-v2__link').on('click',function(e){
	var tabText = $(this).text();
	e.preventDefault();
	$(this).closest('.select-v2__item').addClass('is-active').siblings().removeClass('is-active');
	$('.select-v2__list, .select-v2__btn').removeClass('is-active');
	$('.select-v2__btn').text(tabText)
}).on('mouseenter',function(){
	$(this).closest('.select-v2__item').addClass('is-hover');
}).on('mouseleave',function(){
	$(this).closest('.select-v2__item').removeClass('is-hover');
});







// 모바일 gnb
function gnbScroll(){
	var gnbState = 0;
	var gnbIndex = 0;
	var gnbScrollTop = 0;
	var gnbHead, gnbTop, gnbScroll;
	var $item = $('.m-gnb__head').find('.js-link');
	var $head = $('.m-gnb__depth1').find('.js-head');
	var $set = $('.m-gnb__head');
	var $body = $('.m-gnb__depth1');
	var listCnt = $item.length
	if($body.size() != 0){
		if ($set.find('.is-active').length == 0) {
			$item.eq(gnbIndex).parent().addClass('is-active').siblings().removeClass('is-active');
		}
		gnbSetFnc();
	}

	$body.on('scroll',function(){
		gnbHead = [];
		gnbTop = $body.offset().top;
		gnbScroll = $body.scrollTop();
		$head.each(function(){
			gnbHead.push($(this).offset().top - gnbTop + gnbScroll);
		});
		if(gnbScrollTop < $(this).scrollTop()){
			gnbScrollTop = $(this).scrollTop();
			if(gnbScrollTop >= gnbHead[gnbIndex + 1]){
				gnbIndex++;
				gnbSeleFnc();
			}
		}else{
			gnbScrollTop = $(this).scrollTop();
			if(gnbScrollTop < gnbHead[gnbIndex]){
				gnbIndex--;
				gnbSeleFnc();
			};
		};
	});

	$item.each(function(idx){
		$(this).on('click',function(e){
			e.preventDefault();
			// if (gnbState == 0) {
			// 	gnbSetFnc();
			// 	gnbState = 1;
			// }
			gnbIndex = idx;
			$body.stop().animate({'scrollTop':gnbHead[gnbIndex]},500,function(){
				$(this).css({'overflow-y':'scroll'})
			});

			gnbSeleFnc();
		});
	})

	function gnbSetFnc(){ //초기 셋팅
		gnbHead = [];
		gnbTop = $body.offset().top;
		gnbScroll = $body.scrollTop();
		$set.scrollTop(0);
		$body.scrollTop(0);
		$head.each(function(){
			gnbHead.push($(this).offset().top - gnbTop + gnbScroll);
		});
	}

	function gnbSeleFnc(){
		$item.eq(gnbIndex).parent().addClass('is-active').siblings().removeClass('is-active');
		if(listCnt == gnbIndex+1){
			$item.eq(gnbIndex-1).parent().addClass('is-active').siblings().removeClass('is-active');
		}
	}

	$('.js-event-link').on('click',function(e){
		e.preventDefault();
		$('.js-link').last().trigger('click');
	})
}



//마우스 팝업
$('.js-info').on('mouseenter',function(){
    if ($('body').attr('data-mobile') == 'false'){
        $(this).find('.info-btn').find('.info-btn__wrap').stop().fadeIn(300);
        $('.js-info-motion').addClass('is-active');
    }
}).on('mouseleave',function(){
    if ($('body').attr('data-mobile') == 'false'){
		$(this).find('.info-btn').find('.info-btn__wrap').stop().fadeOut(300);
		$('.js-info-motion').removeClass('is-active');
    }
}).on('click',function(e){
    e.preventDefault();
    if ($('body').attr('data-mobile') == 'true'){
        if($(this).find('.info-btn').hasClass('is-active')){
        }else{
            $(this).find('.info-btn').addClass('is-active').find('.info-btn__wrap').fadeIn(300);
			$(this).closest('.content-box').removeClass('js-scroll-motion')
        }
    }
});

$('.js-close').on('click',function(e){
	e.stopPropagation();
    $(this).closest('.info-btn').removeClass('is-active');
    $(this).closest('.info-btn__wrap').fadeOut(300);
	setTimeout(function() { $(this).closest('.content-box').addClass('js-scroll-motion')},500);
});


$(window).on('load',function(){
	if ($('body').attr('data-mobile') == 'true'){
		try {
			var breadCrumbActive = $('.breadcrumb__item.is-active').offset().left - 18;
			var breadCrumbScroll = $('.breadcrumb__scroll');
			breadCrumbScroll.animate({scrollLeft:breadCrumbActive},0);
		}catch(e){}
	}
});
