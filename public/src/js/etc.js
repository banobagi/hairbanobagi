
(function(){

    $('.etc__link').on('click',function(e){
		e.preventDefault();
		if ($('body').attr('data-mobile') == 'false'){
	        // showScroll()
	        var winWidth = $(window).outerWidth();
			var target = '.etc-section'+$(this).closest('.etc__item').data('target');
			var left = $(target).find('.etc__tit').position().left;
	        var headerW = $('.header').outerWidth();
			// var lnbW = $('.lnb').outerWidth();
			// var etcPadding = $('.etc').css('padding-left').replace(/[^0-9]/g, "");
			var wrapPadding = Number($('.etc__wrap').css('padding-left').replace(/[^0-9]/g, ""));
			var total = $('.horizontal-scroll').width() + winWidth; // 푸터없을시  + winWidth 제거
			var perc = (left) / (total - $(window).width()) * 100;
			var height = $('html').height();
			var final = Math.ceil(((height - $(window).height())/ 100 * perc)) ;
			$('html, body').animate({scrollTop: final + wrapPadding},500);
		}else{
			var headerH = $('.header').outerHeight();
			var target = '.etc-section'+$(this).closest('.etc__item').data('target');
			$('html,body').animate({scrollTop:$(target).offset().top - headerH}, 500);
		}
    });

})();

$('.browser__link').on('mouseenter',function(e){
	if ($('body').attr('data-mobile') == 'false'){
		$(this).closest('.browser__item').addClass('is-hover');
	}
}).on('mouseleave',function(e){
	if ($('body').attr('data-mobile') == 'false'){
		$(this).closest('.browser__item').removeClass('is-hover');
	}
});

$('.browser__item').hoverdir({
    hoverElem: '.browser__bg'
});
