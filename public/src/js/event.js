

(function(){
	const lnbLine = document.querySelector('.js-lnb-line');
	const selectLine = document.querySelector('.js-select-line');
	function showValue(){
		var headerH = $('.header').height();
		try{var lnbY = lnbLine.getBoundingClientRect().top;}catch(e){}
		try{var selectY = selectLine.getBoundingClientRect().top;}catch(e){}
	    if ($('body').attr('data-mobile') == 'false') return false;
        if(lnbY < headerH){
			$('.lnb__list').addClass('is-active');
    	}else{
			$('.lnb__list').removeClass('is-active');
		}
		if(selectY < headerH){
			$('.lnb__nav').addClass('is-active');
    	}else{
			$('.lnb__nav').removeClass('is-active');
		}
	}
	$(window).on('scroll resize load',function(){
		showValue();
	});
})();


$(window).on("load resize", function () {
    if ($('body').attr('data-mobile') == 'true'){
        if ( !$('.js-slide-banner').hasClass('slick-initialized')){
             $('.js-slide-banner').slick({
                autoplay:false,
                autoplaySpeed:3000,
                slidesToShow:1,
                slidesToScroll: 1,
                arrows:false,
                pauseOnHover:false,
                pauseOnFocus:false,
                zIndex:10,
				dots:true,
			    dotsClass:'event-dot',
            });
        }
    }else{
        if ( $('.js-slide-banner').hasClass('slick-initialized')){
             $('.js-slide-banner').slick('unslick');
         }
    }
});
